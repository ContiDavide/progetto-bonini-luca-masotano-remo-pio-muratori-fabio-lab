package model.entities;

/**
 * 
 * Defines the properties that can be modified by items.
 *
 */
public enum Property {

    /**
     * The life of a certain character.
     */
    LIFE,

    /**
     * The damage of bullets shot by a certain character.
     */
    DAMAGE,

    /**
     * The range of bullets shot by a certain character.
     */
    RANGE,

    /**
     * The fire rate of a certain character.
     */
    FIRERATE;

}
