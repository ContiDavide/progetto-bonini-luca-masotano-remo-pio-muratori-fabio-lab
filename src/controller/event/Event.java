package controller.event;

/**
 * Represents an event.
 */
public interface Event {

    /**
     * Get the message of this event.
     * 
     * @return the event's message
     */
    String getMessage();

}
